-- task 2.1 -- 
/*2-1.1. Створити процедуру для зміни прізвища
студента з певним прізвищем відповідно до вказаного айдішника*/
Go
CREATE PROC ChangeMiddleName
@a nvarchar(15), @i INT
AS
UPDATE Student SET middle_name = @a
where id_student = @i
/*Передача данних в процедуру ChangeMiddleName*/
exec ChangeMiddleName @a='Levchenko', @i=1
/*Видалити процедуру ChangeMiddleName*/
Drop Procedure ChangeMiddleName

/*2-1.2. Створити процедуру для зміни номеру факультету,
що вказує користувач за вказаною назвою факультета*/
GO
CREATE PROC ChangeFacultyNumber
@name nvarchar(50), @dpn int
AS
BEGIN
UPDATE Faculty SET dekan_phone_number= @dpn
WHERE name_faculty = @name
END
/*Передача данних в процедуру ChangeFacultyNumber*/
EXEC ChangeFacultyNumber N'FIKT', N'098647381'

/*Видалити процедуру ChangeFacultyNumber*/
DROP PROCEDURE ChangeFacultyNumber

/*2-1.3 Створити процедуру для зміни номеру департамента,
що вказує користувач за вказаною айді депертамента*/
GO
CREATE PROC ChangeDepartmentNumber
@i INT, @pd INT
AS
BEGIN
UPDATE Departments SET phone_department= @pd
WHERE id_department = @i
END
/*Передача данних в процедуру ChangeFacultyNumber*/
EXEC ChangeDepartmentNumber @i=1, @pd=N'098647381'

/*Видалити процедуру ChangeFacultyNumber*/
DROP PROCEDURE ChangeDepartmentNumber


-- task 2.2 -- 
/*1. Тригер, що спрацьовує при видалені Дисципліни
у новоствороену таблицю deleted*/

/*створимо таблицю для видалених записів*/
CREATE TABLE deleted_disc
(
    id_discipline int identity(1, 1) NOT NULL ,
    subject_ nvarchar(50) NOT NULL ,
    id_tutor int NOT NULL ,
);
GO
CREATE TRIGGER discipline_delete
ON Discipline	
AFTER DELETE
AS
INSERT INTO deleted_prac (subject_, id_tutor)
SELECT subject_, id_tutor
FROM deleted_disc
/*Видалити тригер*/
DROP TRIGGER discipline_delete
/*ТЕСТУВАННЯ ТРИГЕРА*/
DELETE FROM Discipline
WHERE id_discipline=1
SELECT * FROM deleted_disc

/*2. Тригер, що спрацьовує при видалені дисципліни
у новоствороену таблицю deleted*/
GO
CREATE TRIGGER disciplineUpdate
ON Discipline
AFTER UPDATE
AS
INSERT INTO deleted_disc (subject_, id_tutor)
SELECT subject_, id_tutor
FROM inserted
/*Видалити тригер*/
DROP TRIGGER disciplineUpdate
/*ТЕСТУВАННЯ ТРИГЕРА*/
INSERT INTO Discipline (subject_, id_tutor)
Values ('Frontend', 8);
UPDATE Discipline SET subject_ = 'Backend'
where id_tutor = 8;
select*from deleted_disc