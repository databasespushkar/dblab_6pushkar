--task 1.1 - 1.	Розрахунок середнього балу студентів за період – без урахування перездач.
CREATE PROC midScope AS 
SELECT AVG(reiting.reiting) FROM Reiting
GO
exec midScope

--task 1.2 - 2.	Розрахунок середнього балу студентів за період – з урахування перездач.
CREATE PROC AvgPeriod2
AS
SELECT dbo_student.Fname, AVG(Reiting.Reiting) as AVGmark
FROM Reiting
join dbo_student on dbo_student.Kod_stud = Reiting.Kod_student and
Reiting.K_zapis IN ( SELECT Rozklad_pids.K_zapis FROM Rozklad_pids WHERE
Rozklad_pids.Zdacha_type = 'true')
Group by dbo_student.Fname
GO
exec AvgPeriod2

--task 1.3 - Визначення студентів, що навчаються на 4 та 5.
CREATE PROC FindGoodStudents
AS
SELECT Reiting.Kod_student, Reiting.K_zapis
FROM Reiting
WHERE Reiting.Kod_student IN (
SELECT Kod_student FROM (
SELECT AVG(Reiting.Reiting) AS AvgM, Reiting.Kod_student FROM Reiting GROUP BY
Reiting.Kod_student HAVING AVG(Reiting.Reiting) BETWEEN '74' AND '100') AS
Kod_student)
EXEC FindGoodStudents

--task 1.4 - Процедура виводить суму балів та її значення в національній системі та ECTS.
CREATE PROC OpenMarks
AS
Select dbo_student.Name_ini, sum(Reiting.Reiting) as mark, case
WHEN sum(Reiting.Reiting) > 90 THEN 'відмінно'
WHEN sum(Reiting.Reiting) BETWEEN 85 AND 89 THEN 'добре'
WHEN sum(Reiting.Reiting) BETWEEN 75 AND 84 THEN 'добре'
WHEN sum(Reiting.Reiting) BETWEEN 65 AND 74 THEN 'задовільно'
WHEN sum(Reiting.Reiting) BETWEEN 60 AND 64 THEN 'задовільно'
when sum(Reiting.Reiting) < 60 then 'незадовільно' end as 'national marks',
case
WHEN sum(Reiting.Reiting) > 90 THEN 'A'
WHEN sum(Reiting.Reiting) BETWEEN 85 AND 89 THEN 'B'
WHEN sum(Reiting.Reiting) BETWEEN 75 AND 84 THEN 'C'
WHEN sum(Reiting.Reiting) BETWEEN 65 AND 74 THEN 'D'
WHEN sum(Reiting.Reiting) BETWEEN 60 AND 64 THEN 'F'
when sum(Reiting.Reiting) < 60 then 'FX' end as 'ECTS',
predmet.Nazva
from Reiting
join dbo_student on
reiting.Kod_student = dbo_student.Kod_stud
join Rozklad_pids on
Rozklad_pids.K_zapis = reiting.K_zapis
join Predmet_plan on
Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
join predmet on
predmet.K_predmet = Predmet_plan.K_predmet
GROUP BY dbo_student.Name_ini, predmet.Nazva, dbo_student.Fname

EXEC OpenMarks

--task 1.5 - Тригер на вставку даних в таблицю студент – якщо код групи новий в таблицю додається група.
alter TRIGGER InsertStudent ON dbo_student INSTEAD OF INSERT AS
    BEGIN
    DECLARE @group VARCHAR(7)
    SELECT @group = Kod_group FROM inserted
    IF NOT (@group IN (SELECT dbo_groups.Kod_group FROM dbo_groups))
    INSERT INTO dbo_groups(Kod_group, K_navch_plan ) VALUES (@group, 1);
    INSERT INTO dbo_student (Sname, Name, Fname, N_ingroup, Kod_group)
    SELECT Sname, Name, Fname, N_ingroup, Kod_group FROM inserted
    END
    INSERT INTO dbo_student(Sname, Name, Fname, N_ingroup, Kod_group)
    VALUES('Лопатюк', 'Дмитро', 'Воолодимирович', '3', 'ПІ-33')
select * from dbo_student
select * from dbo_groups

--task 1.6 - Тригер на модифікацію даних з таблиці студенти якщо більше немає cтудентів в групі група знищується.
ALTER TRIGGER DeleteStudents ON dbo_student FOR DELETE AS
    BEGIN
    DECLARE @group varchar(8)
    SELECT @group = Kod_group FROM deleted
    IF NOT EXISTS(SELECT dbo_student.Kod_group FROM dbo_student WHERE
    dbo_student.Kod_group = @group)
    BEGIN
        DELETE FROM dbo_groups WHERE dbo_groups.Kod_group = @group
    END
END
DELETE FROM dbo_student WHERE dbo_student.Sname LIKE 'Лопатюк'