
CREATE TABLE Student
(
    id_student int identity(1, 1) NOT NULL ,
    middle_name nvarchar(50) NOT NULL ,
    first_name nvarchar(50) NOT NULL ,
    last_name nvarchar(50) NOT NULL ,
    credit_book int,
    id_group int NOT NULL ,
    id_discipline_1 int NOT NULL ,
    id_discipline_2 int NOT NULL ,
    id_discipline_3 int NOT NULL ,
    id_discipline_4 int NOT NULL ,
    id_discipline_5 int NOT NULL ,
    id_discipline_6 int NOT NULL ,
    CONSTRAINT id_student_pk PRIMARY KEY CLUSTERED (id_student),
);
GO

CREATE TABLE Group_
(
    id_group int identity(1, 1) NOT NULL ,
    group_name nvarchar(50) NOT NULL ,
    speciality nvarchar(50) NOT NULL ,
    id_faculty int NOT NULL ,
    id_tutor int NOT NULL ,
    CONSTRAINT id_group_pk PRIMARY KEY CLUSTERED ( id_group),
);

CREATE TABLE Faculty(
    id_faculty int identity(1, 1) NOT NULL ,
    name_faculty nvarchar(50) NOT NULL ,
    dekan_pib nvarchar(150) NOT NULL ,
    dekan_phone_number int NOT NULL ,
    CONSTRAINT id_faculty_pk PRIMARY KEY CLUSTERED (id_faculty)
);
GO

CREATE TABLE Discipline
(
    id_discipline int identity(1, 1) NOT NULL ,
    subject_ nvarchar(50) NOT NULL ,
    id_tutor int NOT NULL ,
    CONSTRAINT id_discipline_pk PRIMARY KEY CLUSTERED (id_discipline),
);
GO


CREATE TABLE Departments
(
    id_department int identity(1, 1) NOT NULL ,
    name_department nvarchar(50) NOT NULL ,
    phone_department int NOT NULL ,
    id_faculty int NOT NULL ,
    CONSTRAINT id_department_pk PRIMARY KEY CLUSTERED (id_department),
);
GO

CREATE TABLE Tutor
(
    id_tutor int NOT NULL ,
    middle_name nvarchar(50) NOT NULL ,
    first_name nvarchar(50) NOT NULL ,
    last_name nvarchar(50) NOT NULL ,
    phone_number int NOT NULL ,
    id_department int NOT NULL ,
    CONSTRAINT id_tutor_pk PRIMARY KEY CLUSTERED (id_tutor),
);
GO

INSERT Student VALUES (N'Kruv', N'Natalia', N'Sergiivna', N'0054214', 1, 2, 3, 6, 1, 17, 25)
INSERT Student VALUES (N'Luchakivsky', N'Petro', N'Polyanovych', N'0064315', 2, 6, 5, 1, 4, 3, 54)
INSERT Student VALUES (N'Khmelnyk', N'Olena', N'Sergiivna', N'0053267', 2, 8, 43, 4, 1, 23, 16)
INSERT Student VALUES (N'Novosad', N'Andriy', N'Polynovych', N'00534217', 2, 8, 43, 3, 1, 23, 16)
INSERT Student VALUES (N'Derevyanko', N'Cheslav', N'Avgysynovych', N'00532217', 2, , 43, 65, 7, 23, 16)
INSERT Student VALUES (N'Chernyshenko', N'Radan', N'Volodymyrovych', N'00434217', 2, 5, 23, 65, 7, 23, 16)
INSERT Student VALUES (N'Savchenko', N'Granyslav', N'Igorovych', N'0023267', 2, 75, 43, 65, 47, 7, 16)
INSERT Student VALUES (N'Mizernyi', N'Us', N'Rostyslavovych', N'0023267', 2, 5, 43, 65, 47, 6, 16)
INSERT Student VALUES (N'Cherevatenko', N'Yosef', N'Rostyslavovych', N'0023267', 2, 5, 43, 65, 47, 2, 16)

INSERT Group_ VALUES (N'21-2', N'IPZ', 1, 1)
INSERT Group_ VALUES (N'20-2', N'IPZ', 1, 2)
INSERT Group_ VALUES (N'19-4', N'KN', 1, 3)
INSERT Group_ VALUES (N'21-5', N'VT', 1, 3)
INSERT Group_ VALUES (N'19-1', N'KN', 1, 3)

INSERT Faculty VALUES (N'FIKT', N'Levenko Andriy Yanovych',  N'098647381')
INSERT Faculty VALUES (N'FPU', N'Ratushna Yana Sergiivna', N'098657381')
INSERT Faculty VALUES (N'FB', N'Levenko Ostapovych Ostapovych', N'093647381')

INSERT Discipline VALUES (N'Bazy Dannyh', 1)
INSERT Discipline VALUES (N'Programmuvannya movoyu Python', 2)
INSERT Discipline VALUES (N'Programmuvannya movoyu Go', 2)
INSERT Discipline VALUES (N'Frontend rozrobka', 3)

INSERT Departments VALUES (N'FIT', N'380572815', 1)
INSERT Departments VALUES (N'DIM', N'380572814', 2)
INSERT Departments VALUES (N'MIF', N'380572813', 3)

INSERT Tutor VALUES (1, N'Kruv', N'Rozalia', N'Sergiivna', N'0054214415', 1)
INSERT Tutor VALUES (2, N'Butko', N'Radan', N'Levovych', N'0054214415', 2)
INSERT Tutor VALUES (3, N'Rashetnyak', N'Olena', N'Petrivna', N'0054214415', 3)